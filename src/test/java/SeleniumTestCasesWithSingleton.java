import core.BrowserNames;
import core.SingletonBrowserClass;
import element.ReadFiles;
import operations.AdditionalOperation;
import operations.ClickOperation;
import operations.SelectOperation;
import operations.SendKeysOperation;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Random;
import java.util.concurrent.TimeUnit;

public class SeleniumTestCasesWithSingleton {


    public WebDriver driver;
    private static Logger LOGGER;
    SingletonBrowserClass sbc;


    @BeforeClass
    public static void setupClass() {
        LOGGER = LoggerFactory.getLogger(SeleniumTestCases.class);
    }

    @Before
    public void setupTest() {
        sbc = SingletonBrowserClass.getInstanceOfSingletonBrowserClass(BrowserNames.CHROME);
        driver = sbc.getDriver();
        driver.manage().deleteAllCookies();
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        driver.manage().timeouts().pageLoadTimeout(30, TimeUnit.SECONDS);
    }


    @Test
    public void googleTestChrome() throws Exception {

        driver.get("https://testdivalogin.logo.com.tr/");
        LOGGER.info("CHROME IS OPENED...");


        SendKeysOperation sendKeysOperation = new SendKeysOperation(driver);
        ClickOperation clickOperation = new ClickOperation(driver);

        driver.findElement(By.xpath("//*[@id='username']")).sendKeys("faruk.akyol");
        driver.findElement(By.xpath("//*[@id='password']")).sendKeys("aIYqVE");
        driver.findElement(By.xpath("//*[@id='loginBtn']")).click();

        clickOperation.click(By.xpath("//*[text()='Divaportal']"));
        //driver.findElement(By.xpath("//*[text()='Divaportal']")).click();

        driver.switchTo().defaultContent();


        WebDriverWait wait = new WebDriverWait(driver, 10);
        WebElement element = wait.until(
                ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[text()='Stok Kontrol']")));
        element.click();
        driver.findElement(By.xpath("//*[text()='Stok Kartı ']")).click();
        driver.switchTo().defaultContent();
        int size = driver.findElements(By.tagName("iframe")).size();
        System.out.println(size);

        driver.switchTo().frame(1);
        int size1 = driver.findElements(By.tagName("iframe")).size();
        System.out.println(size1);

        driver.switchTo().frame(0);
        System.out.println("faruk akyol");
        clickOperation.click(By.xpath("//span[@id='lblFiyatGirisi']"));


        //driver.close();
        //driver.quit();
        LOGGER.info("BROWSER IS CLOSING...");
    }

    @Test
    public void test2() {

        driver.get("https://testdivalogin.logo.com.tr/");
        LOGGER.info("CHROME IS OPENED...");


        SendKeysOperation sendKeysOperation = new SendKeysOperation(driver);
        ClickOperation clickOperation = new ClickOperation(driver);

        driver.findElement(By.xpath("//*[@id='username']")).sendKeys("faruk.akyol");
        driver.findElement(By.xpath("//*[@id='password']")).sendKeys("aIYqVE");
        driver.findElement(By.xpath("//*[@id='loginBtn']")).click();

        clickOperation.click(By.xpath("//*[text()='Divaportal']"));


        driver.switchTo().defaultContent();


        WebDriverWait wait = new WebDriverWait(driver, 10);
        WebElement element = wait.until(
                ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[text()='Stok Kontrol']")));
        element.click();
        driver.findElement(By.xpath("//*[text()='Stok Kartı ']")).click();
        driver.switchTo().defaultContent();


        int size = driver.findElements(By.tagName("iframe")).size();
        System.out.println("Frame sayisi : " + size);

        for (int i = 0; i <= size; i++) {
            driver.switchTo().frame(i);
            System.out.println("Frame : " + i);
            int total = driver.findElements(By.tagName("iframe)")).size();
            System.out.println("Aranilan Web element sayisi : " + total);
            driver.switchTo().defaultContent();
        }


        //clickOperation.click(By.xpath("//span[@id='lblFiyatGirisi']"));

        //driver.close();
        //driver.quit();
        LOGGER.info("BROWSER IS CLOSING...");


    }


}


