import core.BrowserNames;
import core.ExecuteBrowser;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class SeleniumTestCases {
    public ExecuteBrowser executeBrowser;
    public WebDriver driver;
    private static Logger LOGGER;




    @BeforeClass
    public static void setupClass() {
        LOGGER = LoggerFactory.getLogger(SeleniumTestCases.class);
    }

    @Before
    public void setupTest() {
        executeBrowser = new ExecuteBrowser();
        driver = executeBrowser.launchBrowser(BrowserNames.CHROME);
        driver.manage().window().maximize();
    }


    @Test
    public void googleTestChrome() throws InterruptedException {

        LOGGER.info("a test message");
        driver.get("https://www.google.com");
        Thread.sleep(2000l);
        driver.close();
    }


}
