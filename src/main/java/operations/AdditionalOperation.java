package operations;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;

public class AdditionalOperation {

    private Logger log = LoggerFactory.getLogger(SendKeysOperation.class);
    private WebDriver driver;

    public AdditionalOperation(WebDriver driver) {
        this.driver = driver;
    }

    public void takeSnapShot(String fileWithPath) throws Exception {

        //Convert web driver object to TakeScreenshot

        TakesScreenshot scrShot = ((TakesScreenshot) driver);

        //Call getScreenshotAs method to create image file

        File SrcFile = scrShot.getScreenshotAs(OutputType.FILE);

        //Move image file to new destination

        File DestFile = new File(System.getProperty("user.dir") +"/screenshots/"+ fileWithPath);

        //Copy file at destination

        FileUtils.copyFile(SrcFile, DestFile);

    }


    public void highLighterElement( By element){
        WebElement ele=driver.findElement(element);
        JavascriptExecutor js = (JavascriptExecutor) driver;
        js.executeScript("arguments[0].setAttribute('style', 'background: green; border: 3px solid blue;');", ele);
    }
}
